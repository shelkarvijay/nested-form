json.extract! contact_persone, :id, :name, :customer_id, :created_at, :updated_at
json.url contact_persone_url(contact_persone, format: :json)
