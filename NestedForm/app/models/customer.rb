class Customer < ApplicationRecord
	has_one :contact_persone, dependent: :destroy
	accepts_nested_attributes_for :contact_persone
	validates :name, presence: true

	def to_s
		name
	end

end

