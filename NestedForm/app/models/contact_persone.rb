class ContactPersone < ApplicationRecord
	belongs_to :customer
	validates :name, presence: true
	def to_s
		name
	end


end
