require 'test_helper'

class ContactPersonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact_persone = contact_persones(:one)
  end

  test "should get index" do
    get contact_persones_url
    assert_response :success
  end

  test "should get new" do
    get new_contact_persone_url
    assert_response :success
  end

  test "should create contact_persone" do
    assert_difference('ContactPersone.count') do
      post contact_persones_url, params: { contact_persone: { customer_id: @contact_persone.customer_id, name: @contact_persone.name } }
    end

    assert_redirected_to contact_persone_url(ContactPersone.last)
  end

  test "should show contact_persone" do
    get contact_persone_url(@contact_persone)
    assert_response :success
  end

  test "should get edit" do
    get edit_contact_persone_url(@contact_persone)
    assert_response :success
  end

  test "should update contact_persone" do
    patch contact_persone_url(@contact_persone), params: { contact_persone: { customer_id: @contact_persone.customer_id, name: @contact_persone.name } }
    assert_redirected_to contact_persone_url(@contact_persone)
  end

  test "should destroy contact_persone" do
    assert_difference('ContactPersone.count', -1) do
      delete contact_persone_url(@contact_persone)
    end

    assert_redirected_to contact_persones_url
  end
end
