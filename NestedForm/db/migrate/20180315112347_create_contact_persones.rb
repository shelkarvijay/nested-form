class CreateContactPersones < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_persones do |t|
      t.string :name
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
