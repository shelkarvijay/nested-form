Rails.application.routes.draw do
  resources :contact_persones
  resources :customers
  root 'customers#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
