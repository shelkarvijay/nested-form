class AddAttachmentAvatarToTables < ActiveRecord::Migration[5.1]
  def self.up
    change_table :tables do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :tables, :avatar
  end
end
