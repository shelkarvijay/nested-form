class AddDataToTable < ActiveRecord::Migration[5.1]
  def change
    add_column :tables, :data, :binary
  end
end
