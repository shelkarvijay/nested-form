class AddTableToPerson < ActiveRecord::Migration[5.1]
  def change
    add_reference :people, :table, foreign_key: true
  end
end
