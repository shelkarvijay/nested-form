class RemoveDataFromTable < ActiveRecord::Migration[5.1]
  def change
    remove_column :tables, :data, :binary
  end
end
