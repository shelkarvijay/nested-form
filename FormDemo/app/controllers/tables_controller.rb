class TablesController < ApplicationController
	def index
		@tables = Table.all
	end

	def show
		@table = Table.find(params[:id])
  	end
	
	def new
		@table = Table.new
		@table.build_person
		#1.times {@table.person.build}
	end

	def create
		@table = Table.create(table_params)

		#1.times {@table.person.build}
		if @table.save
			flash[:notice] = "Thank you for your submission..."
			redirect_to @table
		else
			render 'new'
		end
	end
	def table_params
		params.require(:table).permit(:name, :avatar, person_attributes: [:name, :contact])
	end
end
