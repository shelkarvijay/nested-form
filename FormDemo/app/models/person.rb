class Person < ApplicationRecord
	belongs_to :table
	validates :name, presence: true

	def to_s
		name
	end
end
