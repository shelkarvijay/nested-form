class Table < ApplicationRecord
	has_one :person, dependent: :destroy
	accepts_nested_attributes_for :person
	validates :name, presence: true

	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
	def to_s
		name
	end
end
